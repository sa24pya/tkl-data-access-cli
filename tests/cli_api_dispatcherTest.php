<?php

//namespace DataAccessCLI;

class Cli_api_dispatcherTest extends \PHPUnit_Framework_TestCase
{


    /**
     * App environemnt has impact on congif values
     *
     * @var string
     */
    const APP_ENV = "development";

    /**
     * Server URL
     * @var string
     */
    # Dev-local
    //const SERVER_URL = "http://m2.manager.shobr.com" ;
    const SERVER_URL = "localhost:7001/cli_api_dispatcher.php";

    # QA Server
    //const SERVEL_URL = "data-access.shobr.dtn.com.vn";

    # Mah Server
    //const SERVEL_URL = "api.shobr.com";


    public function setUp()
    {
        $this->populatePersonDB();
    }
    public function tearDown()
    {
        $mongoServer = 'localhost';
        $mongoPort = 27017;
        $manager = new \MongoDB\Driver\Manager("mongodb://$mongoServer:$mongoPort");
        $command = new \MongoDB\Driver\Command([
            'drop' => 'person',
        ]);
        $manager->executeCommand('SA24', $command);
    }

    private function populatePersonDB()
    {
        $person =[
            'client'=>'shobrManager',
            'email'=>'eko@shopall24.com',
            'password'=>'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9',
            'ACLRole'=>'Supplier_Editor',
            'glns'=> ['5790000010974']
        ];
        $dal = DataAccessDAL\DAL::getInstance();
        $dal->personSave((object)$person);
        $person =[
            'client'=>'shobrManager',
            'email'=> rand(0, 5000) . '@' . rand(0, 5000) . '.org',
            'password'=> '123',
            'ACLRole'=> 'Supplier_Editor',
            'glns'=> '11110000010974'
        ];
        $dal = DataAccessDAL\DAL::getInstance();
        return $dal->personSave((object)$person);
    }


    public function test400()
    {
        // echo "\n\n\n\n";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::SERVER_URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            "postvar1=value1&postvar2=value2&postvar3=value3"
        );
        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        // echo " " . __METHOD__ . " Response: ";
        // var_dump($response);
        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $this->assertEquals(400, $info);
    }

    public function test404()
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::SERVER_URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            "serviceName=thisIsAnInvelidServiceName"
        );
        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        // echo " " . __METHOD__ . " Response: ";
        // var_dump($response);

        // header test
        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $this->assertEquals(404, $info);

        // body test
        $subset = ['status'=> '404', 'statusDescription' => 'Not Found' ];
        $jresponse = (array) json_decode($response);
        $this->assertArraySubset($subset, $jresponse);
    }

    public function testPersonAuthenticate400()
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::SERVER_URL);
        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,

            "serviceName=personAuthenticate"
        );
        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        // echo " " . __METHOD__ . " Response: ";
        // var_dump($response);

        // header test
        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $this->assertEquals(400, $info);

        // body test
        $subset = ['status'=> '400', 'statusDescription' => 'Bad Request' ];
        $jresponse = (array) json_decode($response);
        $this->assertArraySubset($subset, $jresponse);
    }


    public function testPersonAuthenticate401()
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::SERVER_URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            "serviceName=personAuthenticate&email=123&password=345"
        );
        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        // echo " " . __METHOD__ . " Response: ";
        // var_dump($response);

        // header test
        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $this->assertEquals(401, $info);
    }

    public function testPersonAuthenticate200()
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::SERVER_URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            "serviceName=personAuthenticate&email=eko@shopall24.com&password=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"
        );
        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        // echo " " . __METHOD__ . " Response: ";
        // var_dump($response);

        // header test
        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $this->assertEquals(200, $info);
    }

    public function testMultiplePersonAuthenticate200()
    {

        // Authenticate
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,self::SERVER_URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            "serviceName=personAuthenticate&email=eko@shopall24.com&password=123"
        );
        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response1 = curl_exec($ch);
        $response2 = curl_exec($ch);
        // echo " " . __METHOD__ . " Response: ";
        // var_dump($response);
        // header test
        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $this->assertEquals(200, $info);
        $response1 = (array) json_decode($response1);
        $response2 = (array) json_decode($response2);

        // ServiceList
        $request1['token'] = $response1['token'];
        $request1['serviceName'] = 'serviceList' ;
        $query1 = "serviceName=serviceList" .
            "&token=" . urlencode( json_encode($request1['token']) );

        $request2['token'] = $response2['token'];
        $request2['serviceName'] = 'serviceList' ;
        $query2 = "serviceName=serviceList" .
            "&token=" . urlencode( json_encode($request2['token']) );

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded'
        ));
        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $query1);

        $response1 = curl_exec($ch);
        var_dump($response1);

        // header test
        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $this->assertEquals(200, $info);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $query2);

        $response2 = curl_exec($ch);
        var_dump($response2);

        // header test
        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $this->assertEquals(200, $info);

        curl_close($ch);
    }

    public function testTokenDelete()
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,self::SERVER_URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            "serviceName=personAuthenticate&email=eko@shopall24.com&password=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"
        );
        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);

        // header test
        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $this->assertEquals(200, $info);

        $response = (array)json_decode($response);
        $request = "serviceName=tokenDelete&token=".json_encode($response["token"])."&filters=".json_encode($response["token"]);
        //var_dump($request);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            $request
        );
        $response = curl_exec($ch);
        var_dump($response);
        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);
        $this->assertEquals(200, $info);
    }




    public function testServiceList200()
    {
        // Authenticate
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::SERVER_URL);
        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            "serviceName=personAuthenticate&email=eko@shopall24.com&password=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"
        );
        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        // echo " " . __METHOD__ . " Response: ";
        // var_dump($response);
        // header test
        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_reset($ch);
        curl_close($ch);
        $this->assertEquals(200, $info);
        $response = (array) json_decode($response);
        $this->assertTrue(is_array($response));
        $this->assertTrue(array_key_exists('token', $response));
        $this->assertTrue(!empty($response['token']));
        //var_dump($response);



        // ServiceList
        $request['token'] = $response['token'];
        $request['serviceName'] = 'serviceList' ;
        $query = "serviceName=serviceList" .
            "&token=" . urlencode(json_encode($request['token']));

        // echo "\n Query: $query \n\n";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::SERVER_URL);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded'
        ));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);

        if ($response === false) {
            echo 'Curl error: ' . curl_error($ch);
        }
        // echo " " . __METHOD__ . " Response: ";

        // header test
        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        // echo "$info";
        $this->assertEquals(200, $info);
        curl_close($ch);

        $response = (array) json_decode($response);
        // var_dump($response);
        $this->assertTrue(array_key_exists('queryResult', $response));
        $this->assertTrue(!empty($response['queryResult']));
        $queryResult = (array)$response['queryResult'];
        $this->assertTrue(array_key_exists('personAuthenticate', $queryResult));
        $this->assertTrue(array_key_exists('serviceList', $queryResult));
    }



    public function testStockRead200()
    {
        // Authenticate
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::SERVER_URL);
        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            "serviceName=personAuthenticate&email=eko@shopall24.com&password=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"
        );
        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        // echo " " . __METHOD__ . " Response: ";
        // var_dump($response);
        // header test
        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_reset($ch);
        curl_close($ch);
        $this->assertEquals(200, $info);
        $response = (array) json_decode($response);
        $this->assertTrue(is_array($response));
        $this->assertTrue(array_key_exists('token', $response));
        $this->assertTrue(!empty($response['token']));
        //var_dump($response);

        // stockRead
        $request['token'] = $response['token'];
        $request['serviceName'] = 'serviceList' ;

        $query = "serviceName=stockRead&filters=" . urlencode(json_encode(array("glns"=>"5790000010974"))) .
            "&token=" . urlencode(json_encode($request['token']));

        // echo "\n Query: $query \n\n";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::SERVER_URL);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded'
        ));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        // var_dump($response);
        if ($response === false) {
            echo 'Curl error: ' . curl_error($ch);
        }
        // echo " " . __METHOD__ . " Response: ";
        // print_r($response."\n");
        // header test
        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        // echo "$info";
        $this->assertEquals(200, $info);
        curl_close($ch);

        $response = (array) json_decode($response);

        $this->assertTrue(!empty($response));
    }

    public function testCatalogueItem200()
    {
        // Authenticate
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,self::SERVER_URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            "serviceName=personAuthenticate&email=eko@shopall24.com&password=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"
        );
        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        // echo " " . __METHOD__ . " Response: ";
        var_dump($response);
        // header test
        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $this->assertEquals(200, $info);


        $catalogueOriginal = (object) [
            'catalogue_foo' => 'catalogue bar',
            'tradeItem' => [
                'informationProviderOfTradeItem' => [
                    'gln' => '5790000010974',
                ],
                'tradeItemFoo' => 'tradeItem bar',
                'gtin' => '424242',
            ]
        ];

        $response = (array)json_decode($response);
        $query = "serviceName=catalogueItemSave&token=".json_encode($response['token'])."&document=".json_encode($catalogueOriginal);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);

        $response = curl_exec($ch);

        var_dump($response);

        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $this->assertEquals(200, $info);

        //$filter = ['tradeItem.gtin' => '1111'];
        $filter = ['tradeItem.informationProviderOfTradeItem.gln' => '5790000010974'];
        $response = (array)json_decode($response);

        $query = "serviceName=catalogueItemRead&token=".json_encode($response['token'])."&filters=".json_encode($filter);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);

        $response = curl_exec($ch);

        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $this->assertEquals(200, $info);

        $response = (array)json_decode($response, true);

        $catalogueItemRead = $response['queryResult']['SA24'][0];
        var_dump($catalogueItemRead);
        var_dump((array)$catalogueOriginal);


        $this->assertEquals(
            $catalogueItemRead,
            (array) $catalogueOriginal
        );

    }


    public function testCatalogueItemPush200()
    {
        // Authenticate
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,self::SERVER_URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            "serviceName=personAuthenticate&email=eko@shopall24.com&password=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"
        );
        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        // echo " " . __METHOD__ . " Response: ";
        var_dump($response);
        // header test
        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $this->assertEquals(200, $info);


        $catalogueOriginal = (object) [
            'catalogue_foo' => 'catalogue bar',
            'tradeItem' => [
                'informationProviderOfTradeItem' => [
                    'gln' => '5790001106485',
                ],
                'tradeItemFoo' => 'tradeItem bar',
                'gtin' => '1111',
            ]
        ];

        $response = (array)json_decode($response);
        $query = "serviceName=catalogueItemPush&token=".json_encode($response['token'])."&document=".json_encode($catalogueOriginal);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);

        $response = curl_exec($ch);

        var_dump($response);

        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $this->assertEquals(200, $info);

    }

    public function testCatalogueItemPush404()
    {
        // Authenticate
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,self::SERVER_URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            "serviceName=personAuthenticate&email=eko@shopall24.com&password=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"
        );
        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        // echo " " . __METHOD__ . " Response: ";
        var_dump($response);
        // header test
        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $this->assertEquals(200, $info);


        $catalogueOriginal = (object) [
            'catalogue_foo' => 'catalogue bar',
            'tradeItem' => [
                'informationProviderOfTradeItem' => [
                    'gln' => '5790001106485',
                ],
                'tradeItemFoo' => 'tradeItem bar',
                'gtin' => '1111',
            ]
        ];

        $response = (array)json_decode($response);
        $query = "serviceName=catalogueItemPushWrong&token=".json_encode($response['token'])."&document=".json_encode($catalogueOriginal);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);

        $response = curl_exec($ch);

        var_dump($response);

        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $this->assertEquals(404, $info);

    }

    public function testCatalogueItemPush401()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,self::SERVER_URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $catalogueOriginal = (object) [
            'catalogue_foo' => 'catalogue bar',
            'tradeItem' => [
                'informationProviderOfTradeItem' => [
                    'gln' => '5790001106485',
                ],
                'tradeItemFoo' => 'tradeItem bar',
                'gtin' => '1111',
            ]
        ];

        $token["code"]="123";
        $token["expire"]="123";
        $query = "serviceName=catalogueItemPush&token=".json_encode($token)."&document=".json_encode($catalogueOriginal);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);

        $response = curl_exec($ch);

        var_dump($response);

        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $this->assertEquals(401, $info);

    }

    public function testCatalogueItemPush400()
    {
        // Authenticate
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,self::SERVER_URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            "serviceName=personAuthenticate&email=eko@shopall24.com&password=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"
        );
        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        // echo " " . __METHOD__ . " Response: ";
        var_dump($response);
        // header test
        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $this->assertEquals(200, $info);

        $query = "serviceName=catalogueItemPush";

        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);

        $response = curl_exec($ch);

        var_dump($response);

        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $this->assertEquals(400, $info);

    }

    /*
    public function testCatalogueItem1FilterList200()
    {
        // Authenticate
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,self::SERVER_URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                "serviceName=personAuthenticate&email=eko@shopall24.com&password=123"
                );
        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        // echo " " . __METHOD__ . " Response: ";
        // var_dump($response);
        // header test
        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_reset($ch);
        curl_close($ch);
        $this->assertEquals(200, $info);
        $response = (array) json_decode($response);
        $this->assertTrue(is_array($response));
        $this->assertTrue(array_key_exists('token', $response));
        $this->assertTrue(!empty($response['token']));
        //var_dump($response);



        // catalogueItemList
        $request['token'] = $response['token'];
        $request['serviceName'] = 'catalogueItemRead' ;
        $query = "serviceName=catalogueItemRead" .
                "&token=" . urlencode(json_encode($request['token'])) .
                "&filters=" . urlencode(json_encode(['tradeItem.informationProviderOfTradeItem.gln'=>'5790001106485']));

        // ch open
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,self::SERVER_URL);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/x-www-form-urlencoded'
        ));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        if( $response === false)
        {
            echo 'Curl error: ' . curl_error($ch);
        }
        echo " " . __METHOD__ . " Response: ";

        // header test
        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        echo "$info";
        $response = (array) json_decode($response);
        $this->assertEquals(200, $info);

    }

    /**
     * Catalogueitem search with 2 filters
     * 1 filter catalogueItem.tradeItem.gtin in client call
     * 2 filter tradeItem.informationProviderOfTradeItem.gln
     *   is added by Server's ACL
     */
    /*
    public function testCatalogueItemList2Filters200()
    {
        // Authenticate
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,self::SERVER_URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                "serviceName=personAuthenticate&email=eko@shopall24.com&password=123"
                );
        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        // echo " " . __METHOD__ . " Response: ";
        // var_dump($response);
        // header test
        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_reset($ch);
        curl_close($ch);
        $this->assertEquals(200, $info);
        $response = (array) json_decode($response);
        $this->assertTrue(is_array($response));
        $this->assertTrue(array_key_exists('token', $response));
        $this->assertTrue(!empty($response['token']));
        //var_dump($response);



        // catalogueItemList
        $request['token'] = $response['token'];
        $request['serviceName'] = 'catalogueItemRead' ;
        $query = "serviceName=catalogueItemRead" .
                "&token=" . urlencode(json_encode($request['token'])) .
                "&filters=" . urlencode(json_encode(['catalogueItem.tradeItem.gtin'=>'5790001106485']));

        // ch open
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,self::SERVER_URL);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded'
        ));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        if( $response === false)
        {
            echo 'Curl error: ' . curl_error($ch);
        }
        echo " " . __METHOD__ . " Response: ";

        // header test
        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        echo "$info";
        $response = (array) json_decode($response);
        $this->assertEquals(200, $info);
    }


*/









}

