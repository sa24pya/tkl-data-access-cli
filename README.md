# TKL DataAccessCLI
TKL DataAccessCLI  
TKL DataAccessCLI provides Entry Point for data-access module.  
Support PSR-1, PSR-2, PSR-3, PSR-4.

![sa24 package repository](https://www.dream-destination-wedding.com/images/exotic2.jpg)

## Install

Via Composer

``` bash

$ composer require tkl/data-access-cli
```

## Usage

``` bash
$ 
$ 
```

## Testing

``` bash
$ phpunit
```

## Contributing

Please see [CONTRIBUTING](https://github.com/tkl/data-access-cli/blob/master/CONTRIBUTING.md) for details.

## Credits

- [Wilson Smith](https://github.com/wilsonsmith)
- [All Contributors](https://github.com/tkl/data-access-cli/contributors)

## License

The GNU General Public Licence version 3 (GPLv3). Please see [License File](LICENSE.md) for more information.
