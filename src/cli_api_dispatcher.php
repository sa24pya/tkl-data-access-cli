<?php
/**
 * Data access Dispatcher (the iceberg's spike)
 * Data Access Entry Point
 * The service to be called, the "service call name", is sent
 * within POST parameters.
 *
 * @author mrx
 * @version 1.0.0
 * @package data-access-cli
 * @subpackage cli
 * @see https://docs.google.com/a/shopall24.com/document/d/1z1ekPxx0HVi_8YtbMuZefarKlmxIsdjxIpEfJYFa8RA/edit?usp=sharing
 */


// Free spech https://github.com/Behat/Behat/blob/master/bin/behat
if (is_file($autoload = getcwd() . '/vendor/autoload.php')) {
    require $autoload;
}

if (is_file($autoload = __DIR__ . '/../vendor/autoload.php')) {
    require($autoload);
} elseif (is_file($autoload = __DIR__ . '/../../../autoload.php')) {
    require($autoload);
} else {
    error_log(
        'You must set up the project dependencies, run the following commands:'.PHP_EOL.
        'curl -s http://getcomposer.org/installer | php'.PHP_EOL.
        'php composer.phar install'.PHP_EOL
    );
    exit(1);
}


// Handling the Preflight (enabling x-domain requests)
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    // #header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Headers: Origin");
    header("Access-Control-Allow-Headers: X-Preflight-Request");
    // #header('P3P: CP="CAO PSA OUR"'); // Makes IE to support cookies
    exit;
}

// Load Configuration
$dotenv = new Dotenv\Dotenv(dirname(__DIR__));
$dotenv->load();

// Service name is the API door's key
$key = 'serviceName';
// Service name MUST be sent as $_POST parameter
if (!array_key_exists($key, $_POST)) {
    header("HTTP/1.0 400 Bad Request");
    return ;
}

// API call
$serviceName = $_POST['serviceName'];
$api = new DataAccessAPI\API();
$api->$serviceName($_POST);
